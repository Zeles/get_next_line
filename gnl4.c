#include "get_next_line.h"
#include <stdlib.h>

void			ft_gnllstdel(void *content, size_t size)
{
	char *tmp;

	if (content != NULL)
	{
		tmp = (char*)content;
		ft_strdel(&tmp);
		size = 0;
	}
}

t_list			*ft_lstnewgnl(const char *content, size_t content_size)
{
	t_list *result;

	result = (t_list*)malloc(1 * sizeof(t_list));
	if (result != NULL)
	{
		if (content != NULL && content_size != 0)
		{
			result->content = ft_strsub(content, 0, content_size - 1);
			result->content_size = content_size;
		}
		else
		{
			result->content = NULL;
			result->content_size = 0;
		}
		result->next = NULL;
		return (result);
	}
	return (NULL);
}

t_list			*ft_createlist(const char *str)
{
	t_list	*result;
	t_list	*next;
	int		flag;
	size_t	count;

	flag = 1;
	while (str != NULL && *str != '\0')
	{
		count = 0;
		while (str[count] != '\0' && str[count] != '\n')
			count++;
		if (flag)
		{
			next = ft_lstnewgnl(str, count + 1);
			result = next;
			flag = 0;
		}
		else
		{
			next->next = ft_lstnewgnl(str, count + 1);
			next = next->next;
		}
		str += count + 1;
	}
	return (result);
}

char			*ft_readfile(const int fd)
{
	char	buff[BUFF_SIZE + 1];
	char	*str;
	char	*tmp;
	int		readfilerresult;

	str = ft_strnew(1);
	while ((readfilerresult = read(fd, buff, BUFF_SIZE)) > 0)
	{
		buff[readfilerresult] = '\0';
		tmp = str;
		str = ft_strjoin(str, buff);
		ft_strdel(&tmp);
	}
	return (str);
}

int				get_next_line(const int fd, char **line)
{
	static	t_list	*list[255];
	t_list			*tmp;
	char			buff[1];
	char			*str[255];

	if (fd < 0 || line == NULL || read(fd, buff, 0) < 0)
		return (-1);
	if (list[fd] == NULL)
	{
		str[fd] = ft_readfile(fd);
		if (str[fd] != NULL && *str[fd] != '\0')
			list[fd] = ft_createlist(str[fd]);
		ft_strdel(&str[fd]);
	}
	if (list[fd] != NULL)
	{
		(*line) = ft_strsub(list[fd]->content, 0, ft_strlen(list[fd]->content));
		tmp = list[fd];
		list[fd] = list[fd]->next != NULL ? list[fd]->next : NULL;
		ft_lstdelone(&tmp, ft_gnllstdel);
		return (1);
	}
	return (0);
}