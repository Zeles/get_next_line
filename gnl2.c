#include "get_next_line.h"

char			*ft_readfile(const int fd)
{
	char	buff[BUFF_SIZE + 1];
	char	*str;
	char	*tmp;
	int		readfilerresult;

	str = ft_strnew(0);
	while((readfilerresult = read(fd, buff, BUFF_SIZE)) > 0)
	{
		buff[readfilerresult] = '\0';
		tmp = str;
		str = ft_strjoin(str, buff);
		ft_strdel(&tmp);
	}
	return (str);
}

int				get_next_line(const int fd, char **line)
{
	static char *str[46];
	char		*tmp;
	char buff[1];
	size_t count;

	if (fd < 0 || line == NULL || read(fd, buff, 0) < 0)
		return (-1);
	if (!str[fd])
		str[fd] = ft_readfile(fd);
	while (str[fd] && *str[fd])
	{
		count = 0;
		while (str[fd][count] != '\n' && str[fd][count] != '\0')
			count++;
		if (str[fd][count] == '\n' || str[fd][count] == '\0')
		{
			tmp = str[fd];
			(*line) = ft_strsub(str[fd], 0, count);
			str[fd] = ft_strsub(str[fd], count + 1, ft_strlen(str[fd]) - count);
			ft_strdel(&tmp);
			return (1);
		}
	}
	ft_strdel(&str[fd]);
	return (0);
}