#include "get_next_line.h"

static	int		ft_createline(char **line, char **str)
{
	char	*tmp;
	size_t	count;

	count = 0;
	while ((*str)[count] != '\n' && (*str)[count] != '\0')
		count++;
	if ((*str)[count] == '\n' || (*str)[count] == '\0')
	{
		tmp = (*str);
		(*line) = ft_strsub((*str), 0, count);
		(*str) = ft_strsub((*str), count + 1, ft_strlen(*str) - count);
		ft_strdel(&tmp);
	}
	return (1);
}

int				get_next_line(const int fd, char **line)
{
	static char	*str[4096];
	char		*tmp;
	char		buff[BUFF_SIZE + 1];
	int			result;

	if (fd < 0 || line == NULL || read(fd, buff, 0) < 0)
		return (-1);
	if (str[fd] == NULL)
		str[fd] = ft_strnew(1);
	while ((result = read(fd, buff, BUFF_SIZE)) > 0)
	{
		buff[result] = '\0';
		tmp = *(str + fd);
		str[fd] = ft_strjoin(str[fd], buff);
		ft_strdel(&tmp);
		if (ft_strchr(str[fd], '\n'))
			break ;
	}
	if (str[fd] && *str[fd])
		return (ft_createline(line, &str[fd]));
	else
	{
		ft_strdel(&str[fd]);
		return (0);
	}
}