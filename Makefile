BUILDFOLDER=./build/
BINARY=Get_next_line

all: release

release: check_folder
	gcc libft.a get_next_line.c main.c -I. -o ${BUILDFOLDER}${BINARY}

debug: check_folder
	gcc -g libft.a get_next_line.c main.c -I. -o ${BUILDFOLDER}${BINARY}

check_folder:
	if [ ! -d "build" ]; then \
		mkdir build; \
	fi

fclean:
	rm -rf ${BUILDFOLDER}${BINARY}

re: fclean release

re_d: check_folder fclean debug